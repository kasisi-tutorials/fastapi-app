from fastapi import FastAPI, Body, status
from typing import Annotated
from pydantic import BaseModel, Field

app = FastAPI()

class UserIn(BaseModel):
    name: str 
    age: int 
    gender: str = Field(max=100)

@app.get("/")
async def root():
    """ 
    This is an API testing route
    """
    return {'message': 'Welcome'}

@app.post("/user")
async def create_user(user: UserIn):
    return {'message': f'{user.name} has been created successfully'}

@app.get("/user/{user_id}")
async def get_users(user_id: int):
    return {'message': f'You selected user id {user_id}'}
